massachusetts.instafeed = massachusetts.instafeed || function () {

	function init() {
		instaFeedInit();
	}

	function instaFeedInit(){
		var feed = new Instafeed({
			get: 'tagged',
			tagName: 'DKGOVITA',
			clientId: '8fea768e81c94e918be0292765aa71ce',
			template: '<img src="{{image}}" />'
		});
		feed.run();
	};

	return {
		init:init
	};

}();
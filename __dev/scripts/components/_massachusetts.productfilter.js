massachusetts.productfilter = massachusetts.productfilter|| function () {

	function init() {

        if($('.product-list').length > 0){
            // Init packery
            $('.product-list').packery({
                itemSelector: '.product-teaser',
                gutter: 0,
                transitionDuration: '0.5s'
            });

            $('.product-filters-result span').text(($('.product-list .product-teaser:visible').length) + ' varer');

            toggleFilterOptions();
            clearAllFilters();
            toggleFilter();
            mobileFilter();
        }

    }


    function toggleFilterOptions() {
        // onClick product for toggling filter options
        $('.product-filters .options-group .options-title').on('click', function() {
            var optionsGroup = $(this).closest('.options-group');
            if (!optionsGroup.hasClass('expanded')) {
               $(".options-group.expanded").removeClass('expanded');
                // Remove expanded on all filter options
                $('.product-list .product-filters .options-group').removeClass('expanded');
                // console.log("if");
                // Add it to the specific one
                optionsGroup.addClass('expanded');

                // Create hidden overlay to make it removable
                $('.product-list').append('<div class="dropdown-overlay"></div>');

                $('.product-list').on('click', '.dropdown-overlay', function() {
                    // Close all open dropdowns
                    $('.product-filters .options-group').removeClass('expanded');

                    // Remove overlay
                    $(this).remove();
                });
            }
            else {
                optionsGroup.removeClass('expanded');
                // console.log("Else");
            }
        });
}


function toggleFilter() {
        // onChange product for filters
        $('.product-filters .options-group .expanded-area label input').on('change', function() {
            // Add option to choices
            var option = $(this).parent('label');
            // Filter product list
            filterproductList();
        });
    }

    function mobileFilter() {
        // onChange product for filters
        $('#product-list-filter .btn-toogle-filter').on('click', function() {
            console.log('Mobile-filter');
            $('#product-list-filter').toggleClass('mobile-filter-active');
        });
    }

    $(window).on("resize", function(event){
       $('#product-list-filter').removeClass('mobile-filter-active');
   });

    function clearAllFilters() {
        $('.product-filters .filter-choice-reset').on('click', function() {
            $('.product-filters .options-group .expanded-area label input').removeAttr('checked');
            $('.product-filters .filter-choices .choice').remove();

            if ($('.product-filters .filter-choices-wrapper').hasClass('active')) {
                $('.product-filters .filter-choices-wrapper').removeClass('active');
            }

            // Filter product list
            filterproductList();
            return false;
        });
    }


    function filterproductList() {
        // Show all products
        $('.product-list .product-teaser').show();

        // Map filter options by going through all checkboxes that are checked, and adding them to an array
        var filters = [];
        $.map($('.product-filters .options-group :checked'), function(element) {
            filters.push("." + $(element).attr('value'));
        })

        // Hide products that aren't in the current filter options
        if (filters.length > 0) {
            $('.product-list .product-teaser').not(filters.join(', ')).hide();
        }else{
            $('.product-list .product-teaser').show();
        }

        // Relayout packery
        $('.product-list').packery();

        // Update product count
        var productItemsCount = $('.product-list .product-teaser:visible').length;
        $('.product-filters-result span').text(productItemsCount + ' varer');

        if(productItemsCount == 0){
            $('#noproducts').show('fast');
        } else {
            $('#noproducts').hide('fast');
        }
    }


    if ($('#product-list-filter').length > 0) {
        var productFilters = $('.product-filters');
        var productFiltersHeight = $('.product-filters').height();
        var productFiltersPosition = productFilters.offset().top;
        var filterTopPos = $('#product-list-filter').offset();
        var filterHeight = $('#product-list-filter').outerHeight();
        var filterStartPos = filterTopPos.top - filterHeight;
    }

    function stickyFilter(){
        if ($(window).width() > 320) {
            if ($('#product-list-filter').length > 0) {

                if ($(window).scrollTop() > filterStartPos) {
                    $('#product-list-filter').addClass('scrolled');
                }
                else {
                    $('#product-list-filter').removeClass('scrolled');
                }

            }
        }
    }

    //Dropdown hide functions
/* Anything that gets to the document
will hide the dropdown */
$(document).click(function(){
    $(".options-group.expanded").removeClass('expanded');
});

/* Clicks within the dropdown won't make
it past the dropdown itself */
$(".options-group").click(function(e){
    e.stopPropagation();
});


return {
  init:init,
  stickyFilter: stickyFilter
};

}();
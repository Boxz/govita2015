massachusetts.checkoutflow = massachusetts.checkoutflow || function () {

	function init() {

		checkOutFlowForm();

	}



    // Edit contact and delivery information on _approve page
    $(function() {
        $('.editAddressBtn').on("click", function() {

            $('#changeAddress').fadeIn('fast', function(){
                if($('#del_name').val() != ''){
                    $('#shipping-address').show();
                };
            });

        });

        $('#changeAddress-close').on("click", function() {

            $('#changeAddress').fadeOut('fast');

        });

        $('#updateAddress').click(function(event){
            event.preventDefault();

            $(this).text('Vent venligst...');
            $(this).attr('disabled', 'disabled');

            $this = $(this).parents('form');

            $.ajax({
                url: "/address",
                type: 'POST',
                data: $this.serialize(),
            })
            .done(function (data) {
                setTimeout(function() {
                    window.location.reload();
                }, 1000);
                return;
            });

        });
    });

    $(function() {
        // Create cookie with customer informations if btn with class .savecooke-address is clicked.
        $(".savecookie-address, #updateAddress").click(function () {
            var rememberCheckoutValues = {
                "customerName": $('#order_name').val(),
                "address": $('#order_address').val(),
                "zipcode": $('#order_zipcode').val(),
                "city": $('#order_city').val(),
                "email": $('#email').val(),
                "phone": $('#phone').val(),
            };
            $.cookie('rememberCheckoutCookie', JSON.stringify(rememberCheckoutValues), { expires: 30 });
        });

        if ($.cookie('rememberCheckoutCookie') !== undefined) {
            var jsonParsedValues = JSON.parse($.cookie('rememberCheckoutCookie'));

            $("#order_name").val(jsonParsedValues.customerName);
            $("#order_address").val(jsonParsedValues.address);
            $("#order_zipcode").val(jsonParsedValues.zipcode);
            $("#order_city").val(jsonParsedValues.city);
            $("#email").val(jsonParsedValues.email);
            $("#phone").val(jsonParsedValues.phone);
        };
    });

	function checkOutFlowForm(){
		if($('#payment-address')){
			$('#del_address').change(function() {
				if($(this).is(':checked')){
					$('#shipping-address').show('fast');
				}
				else{
					$('#shipping-address').hide('fast');
				}
			});

			$('#terms_accept').change(function() {
				if($(this).is(':checked')){
					$('#terms_accept_container').addClass('accepted');
					$('#next-step-btn').removeClass('inactive');
				}
				else{
					$('#terms_accept_container').removeClass('accepted');
					$('#next-step-btn').addClass('inactive');
				}
			});

			$('#next-step-btn-address').click(function(e){
				if($('#terms_accept').is(':checked')){

				}
				else{
					alert('Du skal acceptere handels-og-leveringsbetingelserne');
					return false;
				}
			});

			// Label magic
			$('.form-row input').focus(function() {
				$("label[for='"+$(this).attr('id')+"']").addClass('label-active').addClass('show');
				$(this).addClass('focus');
			}).blur(function(){

				$("label[for='"+$(this).attr('id')+"']").removeClass('label-active');
				if ($(this).val().length == 0) {
					$("label[for='"+$(this).attr('id')+"']").removeClass('show');
					$(this).removeClass('focus');
				}
			});
		};
	};



	return {
		init:init
	};

}();

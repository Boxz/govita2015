var massachusetts = massachusetts || {};

@import "libs/jquery-1.11.1.min.js";
@import "libs/respond.min.js";
@import "libs/fastclick.js";
@import "libs/owl/owl.carousel.min.js";
@import "libs/_jquery.scrolllock.js";
//@import "libs/instafeed.min.js";
@import "libs/packery.pkgd.min.js";
@import "libs/layzr.js";
@import "libs/jssocials.js";
@import "libs/jquery.cookie.min.js";
//@import "libs/jquery.visible.min.js"


//@import "bootstrap/_affix.js";
//@import "bootstrap/_alert.js";
//@import "bootstrap/_button.js";
//@import "bootstrap/_carousel.js";
@import "bootstrap/_collapse.js";
//@import "bootstrap/_dropdown.js";
//@import "bootstrap/_modal.js";
//@import "bootstrap/_popover.js";
//@import "bootstrap/_scrollspy.js";
@import "bootstrap/_tab.js";
@import "bootstrap/_tooltip.js";
//@import "bootstrap/_transition.js";

// Components
@import "components/_massachusetts.general.js";
@import "components/_massachusetts.navigation.js";
//@import "components/_massachusetts.instafeed.js";
@import "components/_massachusetts.brandslider.js";
@import "components/_massachusetts.cookie.js";
@import "components/_massachusetts.checkoutflow.js"
@import "components/_massachusetts.productfilter.js";
@import "components/_massachusetts.validator.js";
@import "components/_massachusetts.newsletterpopup.js";


// Doc ready
$(function(){

	// Inits
	massachusetts.general.init();
	massachusetts.brandslider.init();
	massachusetts.checkoutflow.init();
	massachusetts.validator.init();
    massachusetts.newsletterpopup.init();

	// To top scroller
	$('#totop').click(function(){
		$('html,body').animate({ scrollTop: 0 }, 'fast');
	});

	// Basket tooltip
	if(!('ontouchstart' in window)){
		$('#basket-toggler').tooltip();
	}

	var imageToShare = jQuery('#productpage-content img:first').attr('data-src');
	var imageToShare = "http://"+location.host +imageToShare;
	jsSocials.setDefaults("pinterest", "facebook", {
		media: imageToShare
	});
//	console.log(imageToShare);

	// var textToShare = jQuery('#product-page-shortdescription').text();


	// autocomplete zip code
	$(function () {
		/*Extra cool postal to city helper*/
		// console.log("yes");
		$('#order_zipcode').blur(function () {
			// console.log("blured");
			var fieldVal = $(this).val();
			// console.log(fieldVal);
			$.ajax({
				type: "GET",
				url: "http://www.postnr.io/?zipcode=" + fieldVal,
				dataType: 'json',
				async: false,
				success: function (text) {
					var response = text.result;
					$('#order_city').val(response);
				},
				error: function (text) {
                            //console.log("fejl: ", text);
                        }
                    });
		})
	});

	// breadcrumbs or checkout_indicator code
	$(function () {
		var pathname = window.location.pathname;
		// console.log("TEST: " + pathname);
		if(pathname.indexOf('address') > -1) {
			pathname = pathname.substring(1);
			$("#checkout_indicator " + "#" + pathname).addClass('current');
		}
		else if(pathname.indexOf('shipping') > -1) {
			pathname = pathname.substring(1);
			$("#checkout_indicator " + "#" + pathname).addClass('current');
			$("#checkout_indicator " + "#" + pathname).removeClass('future');
		}
		else if(pathname.indexOf('approve') > -1) {
			pathname = pathname.substring(1);
			$("#checkout_indicator " + "#" + pathname).addClass('current');
			$("#checkout_indicator " + "#shipping").removeClass('future');
			$("#checkout_indicator " + "#" + pathname).removeClass('future');
		}
		else if(pathname.indexOf('payment') > -1) {
			pathname = pathname.substring(1);
			$("#checkout_indicator " + "#" + pathname).addClass('current');
			$("#checkout_indicator " + "#shipping").removeClass('future');
			$("#checkout_indicator " + "#approve").removeClass('future');
			$("#checkout_indicator " + "#" + pathname).removeClass('future');
		}
		else{

		}
	});


	jQuery("#share").jsSocials({
		shares: ["email", "twitter", "facebook", "pinterest"]
	});

	// Basket toggler
	$('#basket-toggler').click(function(){
		if($('body').hasClass('toggled-basket')){
			$('body').removeClass('toggled-basket');
			$.scrollLock(false);
		} else {
			$('body').addClass('toggled-basket');
			$.scrollLock(true);
		}
	});

	// Carousels
	$('#product-page-image').owlCarousel({
		singleItem: true,
		lazyLoad : true
	});

	// Product long desc
	$('#product-info-show-desc').click(function(){
		$('#product-page-longdescription').slideDown();
		$(this).addClass('show');
		return false;
	});

	// Make dropdown info for 3 seconds after putting into basket.
	// $('#putinbasket').click(function(){
	// 	event.preventDefault();
	// 	$this = $(this).closest('form');
	// 	$('.alert').addClass('hidden');

	// 	if (parseInt($this.find('.attribute').find(':selected').val()) === 0) {
	// 		$this.find('.attribute').addClass('input-error');
	// 	} else if (parseInt($('.input-amount').val()) <= 0) {
	// 		$('.alert').removeClass('hidden');
	// 		$('.alert').html('Du kan ikke ligge mindre end 1 i kurven.');
	// 	} else if ($('.add-basket').hasClass('getAllowNegativeStock') == false && parseInt($('.input-amount').val()) > parseInt($('.stockStatus .countStock').html())) {
	// 		$('.alert').removeClass('hidden');
	// 		if (parseInt($('.stockStatus .countStock').html()) == 0) {
	// 			$('.alert').html('Der er desværre ikke nogen på lager.');
	// 		} else {
	// 			$('.alert').html('Der er desværre kun ' + $('.stock Status .countStock').html() + ' på lager.');
	// 		}
	// 	} else {
	// 		// $('.overlay').show();
	// 		$.ajax({
	// 			type: 'POST',
	// 			data: $this.serialize(),
	// 			beforeSend: function () {
	// 				console.log("Ajax successful");
	// 				// $this.find('.overlay-product-amount').html(parseInt($this.find('.input-amount').val()));
	// 				// $this.find('.product-overlay').show();

	// 				//basket amount
	// 				if ($('#basket-toggler').attr("data-quantity-count")){
	// 					currentAmount = parseInt($('#basket-toggler').attr("data-quantity-count"));
	// 				} else {
	// 					currentAmount = parseInt("0");
	// 				}
	// 				addedAmount = parseInt($this.find('.input-amount').val());
	// 				$('#basket-toggler').attr("data-quantity-count",currentAmount + addedAmount);
	// 				$('#basket-toggler').addClass('cartfull');
	// 				// $('.basket-content-listitem-amount').html(currentAmount + addedAmount + " stk.");


	// 				// Basket total price
	// 				if ($('.basket-text').attr("data-total-price")){
	// 					// currentAmount = parseInt($('.basket-text').attr("data-total-price"));
	// 					console.log("Total: DKK 459,00");
	// 				} else {
	// 					console.log("Kurven er tom");
	// 				}
	// 				// $('.basket-text').html('Gå til kurven');

	// 			}
	// 		});
	// 	}
	// });

	$('#review').click(function(){
		var getReview = $(this).attr("data-target");
		getHeightDescription = $('#product-info-description').height();
		getHeightPageCta = $('#product-page-cta-container').height();
		// console.log("getReview");
		console.log(getReview);
		$('html, body').animate({ scrollTop:  $(getReview).offset().top + getHeightDescription + getHeightPageCta}, 1000);
	});


	//slide down
	var url = window.location.href;
	var lastPart = url.substr(url.lastIndexOf('#') + 1);
	if (lastPart === "added") {
		img = $('#product-page-image ').find('img:first').attr('src');
		$('#basket-mini #basket-mini-img').attr('src', img);
		title = $("#product-info-container h1").text();
		$('#basket-mini #basket-mini-title').text(title);

		$('#basket-mini').delay(1000).slideDown();
		$('#basket-mini').delay(3000).slideUp(function() {
			$('#basket-mini').addClass("hidden");
		});
	}



	// vidcontainer
	// <div class="vidcontainer">
	$('iframe[src*="youtube.com"],iframe[src*="vimeo.com"]').each(function() {
		$(this).wrap( "<div class='vidcontainer'></div>" );
	});

	//collapsable
	$('.subcat-expander').click(function(){
		$(this).find('i').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
		$(this).parent('li').children('ul').toggleClass('expanded');
	});

});

$(window).load(function () {
	massachusetts.navigation.init();
	massachusetts.productfilter.init();


	// Lazy loader
	var layzr = new Layzr();

	//filterScrolled();
});

var instaGramFeedHasLoaded = false;

$(window).scroll(function() {
	massachusetts.productfilter.stickyFilter();

	/*if ($('#instafeed-container').is(':visible') && !instaGramFeedHasLoaded) {
		massachusetts.instafeed.init();
		instaGramFeedHasLoaded = true;
	}*/

});


